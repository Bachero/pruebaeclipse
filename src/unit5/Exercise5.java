package unit5;

public class Exercise5 {

	public static void main(String[] args) {

		Swithboard sb = new Swithboard(2);
		sb.registerCall(new Call(111, 333, 15, 1, false));
		sb.registerCall(new Call(254, 444, 11, 1, false));
		sb.registerCall(new Call(555, 666, 12, 2, false));
		sb.print();
		System.out.println("-----------");
		sb.print(0);
		System.out.println("Num calls: " + sb.getNumRegisteredCalls());
		System.out.println("Turnover: " + sb.getTurnOver());
	}

}
