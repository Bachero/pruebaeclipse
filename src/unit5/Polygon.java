package unit5;

public class Polygon {
	
	private Point[] points;
	
	public Polygon(Point[] points) {
		this.points = points;
			
	}
	
	public Polygon() {
		points = new Point[0];
	}
	
	@Override
	public String toString() {
		String s="";
		for (int i = 0; i < points.length;i++) {
			s += points[i].toString()+"-";	
		}
		s += points[0];
		return s;
			
	}
	
	public void setOffset(int offX, int offY) {
		
		for (int i = 0; i < points.length; i++){
			points[i].setOffset(offX, offY);;
		
		}
		
	}
	
	public double getLenght() {
		double length = 0.0;
		Point p1 = points[0];
		
		for (int i = 1; i < points.length ; i++) {
			Segment s = new Segment(p1, points[i]);
			length += s.module();
			p1 = points[i];
		}
		Segment s = new Segment (points[points.length - 1], points[0]);
		length += s.module();
		return length;
			
			
			
		}
}

