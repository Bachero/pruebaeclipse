package unit5;

public class MainClass5 {

	public static void main(String[] args) {

		Point[] array;
		array = new Point[10];
	
		
		for(int i = 0; i < array.length; i++) {
			array[i] = new Point(i,i);
			System.out.println(array[i]);
		}
		
		for(Point p: array) {
			System.out.println(p);
		}
		
		Rectangle[] arrayR;
		arrayR = new Rectangle[10];
		
		for(int i = 0; i < arrayR.length; i++) {
			arrayR[i] = new Rectangle(array[i],1,1);
		}
		
		for (Rectangle r: arrayR) {
			System.out.println(r);
		}
		
		Rectangle[] rArray;
		rArray = new Rectangle[10];
		
		for(int i = 0; i < rArray.length; i++) {
			rArray[i] = new Rectangle(new Point(i,i),1,1);
			System.out.println(rArray[i]);
		}
		
	}
}
