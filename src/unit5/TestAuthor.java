package unit5;

import java.util.Scanner;

public class TestAuthor {

	public static void main(String[] args) {

		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter the name of the author:");
		String n = input.nextLine();
		System.out.println("Enter the email:");
		String e = input.nextLine();
		
		System.out.println("Enter m or f:");
		char c = input.nextLine().charAt(0);
		input.close();
		
		
		Author a = new Author(n, e, c);
		System.out.println(a.toString());
		
		
	}

}
