package unit5;

import java.util.Scanner;

public class WordGuess {
	
		public static int ATTENTS = 6; 
		public static boolean gameOver = false;
		
	public static void main(String[] agrs) {
		MagicWord magicWord = new MagicWord();
		Scanner input = new Scanner(System.in);	
		
		while(!gameOver) {
			System.out.println("Enter a Character:");
			String s = input.nextLine();
			magicWord.check(s);
			gameOver = magicWord.checkWin();
			System.out.println(s = magicWord.toString());
			ATTENTS --;
			printWin(magicWord);
		}
		input.close();
		

	}

	public static void printWin(MagicWord m) {		
		
		if (ATTENTS == 0) {
			System.out.println("You Lose");
			gameOver = true;
			System.out.println(m.getWord());
		} else {
			if(m.checkWin()) {
				System.out.println("You Win");
				gameOver = true;
			}	
		}
	}
	
}
