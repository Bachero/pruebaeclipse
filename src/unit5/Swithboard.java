package unit5;


public class Swithboard {

	private Call[] calls;
	private int numCall;
	
	public Swithboard(int numMaxCalls) {
		calls = new Call[numMaxCalls];
		numCall = 0;
	}
	
	
	public void registerCall(Call call) {
		if (numCall >= calls.length) {
			System.err.println("Exceded max number of calls.");
		} else {
			calls[numCall] = call;
			numCall ++;
		}
	}
	
	public void print() {
		
		for ( int i = 0; i < numCall; i++) {
			System.out.println(calls[i]);
		}
	}
	
	public void print(int position) {
			if (position >=0 && position<numCall) {
				System.out.println(calls[position]);
			}
	}
	
	public int getNumRegisteredCalls() {
		return numCall;
	}
	
	public double getTurnOver() {
		double acum = 0;
		for ( int i = 0; i < numCall; i++) {
			 acum += calls[i].getCost();
		}
		return acum;
	}
	
	

}
