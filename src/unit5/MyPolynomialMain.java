package unit5;

public class MyPolynomialMain {

	public static void main(String[] args) {
		
		MyPolynomial p1 = new MyPolynomial(1.1, 2.2, 3.3);  
		MyPolynomial p2 = new MyPolynomial(1.1, 2.2, 3.3, 4.4, 5.5); 
		// Can also invoke with an array  
		double[] coeffs = {1.2, 3.4, 5.6, 7.8}; 
		MyPolynomial p3 = new MyPolynomial(coeffs);
		MyPolynomial p4 = new MyPolynomial("poly");
		System.out.println(p4.toString());
		System.out.println(p4.evaluate(2));
		System.out.println(p2.toString());
		System.out.println(p4.toString());
		MyPolynomial p5 = p4.add(p2);
		System.out.println(p5.toString());
		MyPolynomial p6 = p4.addVictor(p2);
		System.out.println(p6.toString());
		
		System.out.println("-------------------------------------------------------------------");
		MyPolynomial p7 = new MyPolynomial(1.1, 2.2, 3.3);  
		MyPolynomial p8 = new MyPolynomial(2); 
		System.out.println(p7.toString());
		System.out.println(p8.toString());
		MyPolynomial p9 = p7.multiply(p8);
		System.out.println(p9.toString());
	}

}