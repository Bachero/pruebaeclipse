package unit5;

public class Segment {
	
	
	private Point startPoint;
	private Point endPoint;
	
	public Segment () {
		startPoint = new Point();
		endPoint = new Point();
	}
	
	public Segment (Point startPoint, Point endPoint) {
		this.startPoint = startPoint;
		this.endPoint = endPoint;
	}
	
	public void setStartPoint(Point p) {
		startPoint = p;
	}
	
	public void setEndPoint(Point p) {
		endPoint = p;
	}
	
	
	public Point getStartPoint() {
		return startPoint;
	}

	public Point getEndPoint() {
		return endPoint;
	}

	public void setOffset(int offX, int offY) {
		startPoint.setOffset(offX, offY);
		endPoint.setOffset(offX, offY);
		
		
	}
	
	
	public double module() {
		int c1 = endPoint.getX() - startPoint.getX();
		int c2 = endPoint.getY() - startPoint.getY();
		double distance = Math.sqrt(c1 * c1 + c2 * c2);
		return distance;
		
	}
	
	@Override
	public String toString() {
		return startPoint.toString() + " - " + endPoint.toString();
	}

}
