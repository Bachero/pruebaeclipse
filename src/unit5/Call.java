package unit5;

public class Call {

	private int numSource;
	private int destinationNumber;
	private int seconds;
	private int timeBand;
	private boolean local;
	private double cost;
	
	public static final double LOCAL_COST = 0.15;
	public static final double BAND_1_COST = 0.20;
	public static final double BAND_2_COST = 0.25;
	public static final double BAND_3_COST = 0.30;
	
	
	public Call(int numSource, int destinationNumber, int seconds, int timeBand, boolean local) {
		this.numSource = numSource;
		this.destinationNumber = destinationNumber;
		this.seconds = seconds;
		this.timeBand = timeBand;
		this.local = local;
		cost = calculateCost();
	}
	
	@Override
	public String toString() {
		return "N. source: " + numSource + " n. dest: " + destinationNumber + " Seconds:  " + seconds + "  Time Band:  " + timeBand; 
	}
	
	
	public double getCost() {
		return cost;
	}

	private double calculateCost() {
		if (local) {
			return seconds * LOCAL_COST;
		} else {
			switch (timeBand) {
				case 1: return seconds * BAND_1_COST;
				case 2: return seconds * BAND_2_COST;
				case 3: return seconds * BAND_3_COST;
			default:
				return -1;
			}
		}
		
		
	}
	
}
