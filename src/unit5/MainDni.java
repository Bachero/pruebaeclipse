package unit5;

public class MainDni {

	public static void main(String[] args) {

		DNI d1 = new DNI(20917943);
		System.out.println(d1);
		
		DNI d2 = new DNI("20.917.943-H");
		System.out.println(d2);
		System.out.println(d2.isCorrect());
		
		DNI d3 = new DNI(2698, 'H');
		System.out.println(d3.toFormattedString());

	}

}
