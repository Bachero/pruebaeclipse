package unit5;


public class MagicWord {

	private boolean[] guessed;
	private String word;
	public static final int COUNTER = 0;
	private static final String[] WORD_ARRAY = {"sacapuntas", "lapiz",
												"goma", "tarta", "coche",
												"moto"};
	
	
	public MagicWord() {
		int numAlea = (int) (Math.random()*WORD_ARRAY.length);
		word = WORD_ARRAY[numAlea];
		guessed = new boolean[word.length()];
		for ( int i=0; i<guessed.length; i++) {
			guessed[i] = false;
		}
		
	}
	
	@Override
	public String toString() {
		String s = "";
		for (int i = 0; i < word.length(); i++) {
			if (!guessed[i]) {
				s = s + "_";
			} else {
				s = s + word.charAt(i);
			}
		}	
		return s;	
	}
	
	public String getWord() {
		return word;
		
	}
		
	
	
	public void check(String s) {
		if (s.length() == 1) {
			checkChar(s.charAt(0));
		} else {
			checkString(s);	
		}
	}
	
	public void checkChar(char c) {
		
		for (int i = 0; i < word.length(); i++) {
			if (c == word.charAt(i)) {
				guessed[i] = true;
			}
		}
		
	}
	
	public void checkString(String s) {
		
		if (s.equals(word)) {
			for (int i = 0; i < word.length(); i++) {
				guessed[i] = true;
			}
		}
	}
		
	
	public boolean checkWin() {
		for (int i = 0; i < word.length(); i++) {
			if (!guessed[i]) {
				return false;
			}
		}	
		return true;
	}	
			
	
}
	

	

